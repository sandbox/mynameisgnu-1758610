Ini Meta Tags allows to define meta tags (keywords, description and copyright) as per the domain and the url called. This is very practical in case of multi-domain installations.

INSTALLATION AND USAGE
----------------------

1. Enable the module normally

2. Modify the Ini file as per your needs. The global section defines meta tags for wherever nothing is defined.
To define a meta tag for a domain and a url, just fill up the appropriate section.
for example :
[drupal.com]
keywords["/"] = "keywords for home page"
description["/"] = "description for home page"

keywords["/projects/ini_meta"] = "keywords for that specific url"
description["/projects/ini_meta"] = "Description for the specific url"


3. The meta tags file has to be in the module directory. 
I know it is dirty and all, but I don't have the time to improve it for now. I have planned to improve that in the future by adding a configuration tab in the admin section. Let's see when I can do it.

This module has been developed by Kevin Muller (kevin@enova-tech.net)
The module has been generously sponsored by our customer NeemranaHotels (http://neemranahotels.com)

